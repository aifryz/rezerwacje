from django import forms
from .models import Booking, Lodging, City


class BookingForm(forms.ModelForm):
    class Meta:
        model = Booking
        fields = '__all__'


class LodgingForm(forms.ModelForm):
    class Meta:
        model = Lodging
        fields = '__all__'


class SearchForm(forms.Form):
    date_from = forms.DateField()
    date_to = forms.DateField()
    city = forms.ModelChoiceField(queryset=City.objects,
                                  empty_label="(Nothing)")
