from django.conf.urls import include, url
from django.contrib import admin
from . import views

app_name = 'page'
urlpatterns = [
    url(r'^index', views.index, name='index'),
    url(r'^find/(?P<city_id>[0-9]+)/'
        r'(?P<date_from>\d{4}-\d{1,2}-\d{1,2})/'
        r'(?P<date_to>\d{4}-\d{1,2}-\d{1,2})',
        views.find, name='find'),
    url(r'^booking_status/', views.book, name='book'),
    url(r'^$', views.index, name='index')

]
