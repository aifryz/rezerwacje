from django.contrib import admin
from django import forms
from .models import City, Lodging, User, Booking
from .forms import BookingForm, LodgingForm
from django.contrib.auth.models import User as dUser
from django.contrib.auth.models import Group as dGroup
from datetime import datetime


class BookingAdmin(admin.ModelAdmin):
    form = BookingForm
    fieldsets = (
        (None, {
            'fields': ['lodging', 'user', ('date_from', 'date_to')]
        }
        ),
        ('Other informations', {
            'fields': ['canceled'],
            'classes': ('collapse',),
        }
        )
    )

    def make_uncanceled(self, request, queryset):
        queryset.update(canceled=False)
    make_uncanceled.short_description = "Rebook selected"

    def make_canceled(self, request, queryset):
        queryset.update(canceled=True)
    make_canceled.short_description = "Cancel selected bookings"

    def status(self, obj):
        if(obj.canceled):
            return "Canceled"
        if(obj.date_from <= datetime.now().date() and
           datetime.now().date() <= obj.date_to):
            return "Checked-in"
        if(obj.date_to < datetime.now().date()):
            return "Checked-out"
        return 'New'

    list_display = ['lodging', 'date_from', 'date_to', 'status']
    actions = [make_canceled, make_uncanceled]


class BookingInline(admin.TabularInline):
    model = Booking
    form = BookingForm
    extra = 0


class LodgingsInline(admin.TabularInline):
    fields = ('address', 'available_from', 'available_to')
    form = LodgingForm
    model = Lodging
    extra = 0


class CityAdmin(admin.ModelAdmin):
    fields = ['name']
    inlines = [LodgingsInline]


class LodgingAdmin(admin.ModelAdmin):
    inlines = [BookingInline]
    fieldsets = (
        (None, {
            'fields': ['address', 'city', ('available_from', 'available_to')]
        }
        ),
        ('Other informations', {
            'fields': ['available'],
            'classes': ('collapse',),
        }
        )
    )

    form = LodgingForm

    def make_unavailable(self, request, queryset):
        queryset.update(available=False)
        for lodging in queryset:
            print(lodging)
            lodging.available = False
            lodging.save()
            bl = Booking.objects.filter(lodging_id=lodging.pk,
                                        date_from__gte=datetime.now().date())
            for booking in bl:
                print(booking)
                booking.canceled = True
                booking.save()
    make_unavailable.short_description = \
        "Cancel all scheduled bookings for that lodging"

    def status(self, obj):
        if(not obj.available):
            return "Unavailable"
        if(obj.available_from < datetime.now().date() < obj.available_to):
            return "Available"
        else:
            return "Unavailable"
    actions = [make_unavailable]
    list_display = ['address',
                    'city',
                    'available_from',
                    'available_to',
                    'status']


admin.site.unregister(dUser)
admin.site.unregister(dGroup)

admin.site.register(City, CityAdmin)
admin.site.register(User)
admin.site.register(Lodging, LodgingAdmin)
admin.site.register(Booking, BookingAdmin)
