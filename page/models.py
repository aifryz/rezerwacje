from django.db import models
from django.core.exceptions import ValidationError


class City(models.Model):
    name = models.CharField(max_length=90)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'


class Lodging(models.Model):
    address = models.CharField(max_length=200)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    available_to = models.DateField()
    available_from = models.DateField()
    available = models.BooleanField(default=True)

    def clean(self):
        start_date = self.available_from
        end_date = self.available_to
        if(start_date > end_date):
            mesg = "Available from date can't be after available to"
            raise ValidationError(mesg)

    def __str__(self):
        return self.address


class User(models.Model):
    nickname = models.CharField(max_length=100)

    def __str__(self):
        return self.nickname


class Booking(models.Model):
    lodging = models.ForeignKey(Lodging, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_from = models.DateField()
    date_to = models.DateField()
    canceled = models.BooleanField(default=False)

    def clean(self):
        start_date = self.date_from
        end_date = self.date_to

        # Check order of dates
        if(start_date > end_date):
            msg = "Reservation start date cant be after reservation end date"
            raise ValidationError(msg)
        # Check if booking is for available date
        if(start_date < self.lodging.available_from):
            msg = "Cannot book this lodging before it's available"
            raise ValidationError(msg)
        if(end_date > self.lodging.available_to):
            msg = "Cannot book this lodging after it's available"
            raise ValidationError(msg)
        # Check if booking does not collide with other bookings
        bookings = Booking.objects.filter(lodging_id=self.lodging.pk)
        for bk in bookings:
            good_before = (start_date <= bk.date_from and
                           end_date <= bk.date_from)
            good_after = (start_date >= bk.date_to and
                          end_date >= bk.date_to)
            same = (bk.pk == self.pk)
            if not(good_before or good_after or same):
                raise ValidationError("This place is already booked from " +
                                      str(bk.date_from) + "to" +
                                      str(bk.date_to))

    def __str__(self):
        return str(self.lodging) + \
            " booked from " + str(self.date_from) + \
            " to " + str(self.date_to)
