from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import dateparse
import datetime
from .models import City, Lodging, Booking, User
from .forms import SearchForm, BookingForm


def find(request, city_id, date_from, date_to):
    date_from = dateparse.parse_date(date_from)
    date_to = dateparse.parse_date(date_to)
    city = City.objects.get(id=city_id)
    bookings = Booking.objects.filter(lodging__city_id=city_id,
                                      lodging__available_from__lte=date_from,
                                      lodging__available_to__gte=date_to) \
                              .exclude(date_from__lte=date_from,
                                       date_to__lte=date_from) \
                              .exclude(date_from__gte=date_to,
                                       date_to__gte=date_to)

    conflicting_lodgings = bookings.values_list('lodging', flat=True)

    good = Lodging.objects.filter(city_id=city_id,
                                  available_from__lte=date_from,
                                  available_to__gte=date_to) \
                          .exclude(id__in=conflicting_lodgings)
    context = {
        'lodgings_list': good,
        'date_from': str(date_from),
        'date_to': str(date_to)
    }
    return render(request, 'page/find.htm', context)


def index(request):
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            city = str(form.cleaned_data['city'].pk)
            date_from = str(form.cleaned_data['date_from'])
            date_to = str(form.cleaned_data['date_to'])
            path = '/app/find/' + city + '/' + date_from + '/' + date_to
            return HttpResponseRedirect(path)
        else:
            return render(request, 'page/index.htm', {'form': form})

    else:
        form = SearchForm()

    return render(request, 'page/index.htm', {'form': form})


def book(request):
    lodging_pk = None
    for key, value in request.POST.items():
        if(value == 'Book'):
            lodging_pk = key

    date_from = dateparse.parse_date(request.POST['date_from'])
    date_to = dateparse.parse_date(request.POST['date_to'])
    name = request.POST['name']
    lodging = get_object_or_404(Lodging, pk=lodging_pk)

    users = User.objects.filter(nickname=name)
    if(users.count() == 0):
        user = User.objects.create(nickname=name)
        user.save()
    else:
        user = users[0]
    bf = BookingForm({'date_from': date_from,
                      'date_to': date_to,
                      'lodging': lodging.pk,
                      'user': user.pk})
    if(bf.is_valid()):
        print('Book valid')
        booking_ok = True
        bf.save()
    else:
        booking_ok = False
        print(bf.errors)

    context = {
        'booking_ok': booking_ok,
        'date_from': date_from,
        'date_to': date_to,
        'lodging': lodging,
        'city': lodging.city,
        'name': name
    }
    return render(request, 'page/booking_status.htm', context)
