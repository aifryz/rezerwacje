from django.test import TestCase
from .models import User, Booking, City, Lodging
from .forms import BookingForm, LodgingForm
from datetime import datetime, timedelta
# Create your tests here.


class BookingFormTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(nickname='Test Testowski')
        city = City.objects.create(name='Testowo1')
        date_from = datetime(year=2017, month=6, day=1)
        date_to = datetime(year=2017, month=6, day=30),
        self.lodging = Lodging.objects.create(address='Testowo1 1',
                                              city=city,
                                              available_from=date_from,
                                              available_to=date_to
                                              )

    def test_positive_booking_duration(self):
        bf = BookingForm({'date_from': '2017-06-02',
                          'date_to': '2017-06-03',
                          'lodging': self.lodging.pk,
                          'user': self.user.pk})
        self.assertTrue(bf.is_valid())

    def test_negative_booking_duration(self):
        bf = BookingForm({'date_from': '2017-06-03',
                          'date_to': '2017-06-02',
                          'lodging': self.lodging.pk,
                          'user': self.user.pk})
        self.assertFalse(bf.is_valid())

    def test_book_before_available(self):
        bf = BookingForm({'date_from': '2017-05-01',
                          'date_to': '2017-05-05',
                          'lodging': self.lodging.pk,
                          'user': self.user.pk})
        self.assertFalse(bf.is_valid())

    def test_book_after_available(self):
        bf = BookingForm({'date_from': '2017-07-01',
                          'date_to': '2017-07-05',
                          'lodging': self.lodging.pk,
                          'user': self.user.pk})
        self.assertFalse(bf.is_valid())

    def test_book_when_already_booked(self):
        date_from = datetime(year=2017, month=6, day=4)
        date_to = datetime(year=2017, month=6, day=8)
        booking = Booking.objects.create(lodging=self.lodging,
                                         user=self.user,
                                         date_from=date_from,
                                         date_to=date_to)
        booking.save()
        bf = BookingForm({'date_from': '2017-06-01',
                          'date_to': '2017-06-10',
                          'lodging': self.lodging.pk,
                          'user': self.user.pk})
        self.assertFalse(bf.is_valid())

    def test_book_before_already_booked(self):
        date_from = datetime(year=2017, month=6, day=4),
        date_to = datetime(year=2017, month=6, day=8)
        book = Booking.objects.create(lodging=self.lodging,
                                      user=self.user,
                                      date_from=date_from,
                                      date_to=date_to)
        book.save()
        bf = BookingForm({'date_from': '2017-06-01',
                          'date_to': '2017-06-02',
                          'lodging': self.lodging.pk,
                          'user': self.user.pk})
        self.assertTrue(bf.is_valid())

    def test_book_after_already_booked(self):
        date_from = datetime(year=2017, month=6, day=4)
        date_to = datetime(year=2017, month=6, day=8)
        book = Booking.objects.create(lodging=self.lodging,
                                      user=self.user,
                                      date_from=date_from,
                                      date_to=date_to)
        book.save()
        bf = BookingForm({'date_from': '2017-06-08',
                          'date_to': '2017-06-12',
                          'lodging': self.lodging.pk,
                          'user': self.user.pk})
        self.assertTrue(bf.is_valid())


class LodgingFormTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(nickname='Test Testowski Lodging')
        self.city = City.objects.create(name='Testowo1Lodging')

    def test_positive_available_lodging(self):
        lf = LodgingForm({'address': 'Aleja Testowa 15',
                          'city': self.city.pk,
                          'available_from': '2017-06-01',
                          'available_to': '2017-06-20'
                          })
        self.assertTrue(lf.is_valid())

    def test_negative_available_duration(self):
        lf = LodgingForm({'address': 'Aleja Testowa 15',
                          'city': self.city.pk,
                          'available_from': '2017-06-28',
                          'available_to': '2017-06-20'
                          })
        self.assertFalse(lf.is_valid())
